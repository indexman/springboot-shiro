package com.southwind.entity;

import lombok.Data;

@Data
public class Account {
    private Integer id;
    private String username;
    private String password;
    // 权限
    private String perms;
    // 角色
    private String role;
}
